def not_so_naive(pattern, text):
    """First we take the length of the pattern and text."""
    p = len(pattern)
    t = len(text)

    """
    Then we look at the equality state
    of the first two letters of the pattern.
    """
    if pattern[0] == pattern[1]:
        """
        If the first two letters of the pattern are equal
        and then the comparison is equal, the pattern will shift to 1.
        If the comparison is not equal, the pattern will be shifted by 2.
        """
        e = 1
        d = 2
    else:
        """
        If the first two letters of the pattern are NOT equal
        and then the comparison is equal, the pattern will shift to 2.
        If the comparison is not equal, the pattern will be shifted by 1.
        """
        e = 2
        d = 1

    """Finally, we start the search process."""
    i = 0
    comparisons = 0  # comparison counter
    indices = []
    while (i <= t - p):
        if text[i+1] != pattern[1]:
            i += d
            comparisons += 1
        else:
            j = 1
            while (j < p and text[i + j] == pattern[j]):
                j += 1
                comparisons += 1
            if j == p and pattern[0] == text[i]:
                indices.append(i+1)
            i = i + e
            comparisons += 1
    if not indices:
        str_indices = '0'
        matches = '0'
        return matches, str_indices, comparisons
    else:
        str_indices = ', '.join(str(i) for i in indices)
        matches = str(len(indices))
        return str_indices, matches, comparisons


def main():
    example = not_so_naive('GCAGAGAG', 'GCATCGCAGAGAGTATACAGTACG')
    matches, indices, comparisons = example
    print('Indices: ', indices)
    print('Number of match: ', matches)
    print('Number of comparisons: ', comparisons)


if __name__ == '__main__':
    main()
