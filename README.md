# Not So Naive

Not So Naive is an algorithm for `Exact String Matching`.

This application is written with python to better understand the course of `String Matching Methods in Text Data`. It provides comparison and matching values.
